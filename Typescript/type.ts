export enum DIRECTION_ORDER_ENUM {
    RIGHT = 0,
    TOP = 1,
    LEFT = 2,
    BOTTOM = 3,
    TURNLEFT = -1,
    TURNRIGHT = -2,
}

export const IDIRECTION = {
    TOP: { x: 0, y: 1 },
    BOTTOM: { x: 0, y: -1 },
    LEFT: { x: -1, y: 0 },
    RIGHT: { x: 1, y: 0 },
};

export const IROTATION = {
    TURNLEFT: 1,
    TURNRIGHT: -1,
};

export interface IPos {
    x: number;
    y: number;
}

export enum RelativeDirection {
    FRONT = "FRONT",
    BACK = "BACK",
    LEFT = "LEFT",
    RIGHT = "RIGHT",
}
